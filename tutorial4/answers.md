#Tutorial 4 Answer
##Fransiscus Emmanuel Bunaren (1806173506)

### Monitoring Metrics
* Explain what is contained in the `/actuator` endpoint.

![/actuator/info](img/0.jpg)

The content of the `/actuator` endpoint is a list of all 
available endpoints in actuator. The data is provided in JSON Format. 

* Try and display some data in the `/actuator/info` endpoint. Any data is fine.

![/actuator/info](img/1.jpg)

### Prometheus
* Try the `/actuator/prometheus` endpoint. Look at its contents and write what you understand about it.

It shows all the properties of current prometheus that we use.

* Change your prometheus.yml file so that it targets the Prometheus metrics endpoint at localhost:8080.
![/actuator/info](img/2.jpg)

### Using the `@timed` annotation
* Try adding a Timed annotation to a function, check it with Prometheus, and explain what you see.

Timed Anotation is used to calculate the time taken by a function when it is being invoked. The graph in prometheus shows 
the number of invocation of a specific function that implements `@Timed` annotation. 

![/actuator/info](img/3.jpg)

