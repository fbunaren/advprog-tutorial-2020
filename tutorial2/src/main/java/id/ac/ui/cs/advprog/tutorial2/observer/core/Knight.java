package id.ac.ui.cs.advprog.tutorial2.observer.core;

public class Knight extends Adventurer{
    public Knight(Guild guild){
        super( "Knight" ,guild);
    }

    @Override
    public void update() {
        this.getQuests().add(this.guild.getQuest());
    }
}
