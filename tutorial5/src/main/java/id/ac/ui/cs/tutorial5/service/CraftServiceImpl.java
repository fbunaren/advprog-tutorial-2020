package id.ac.ui.cs.tutorial5.service;

import id.ac.ui.cs.tutorial5.core.*;
import org.springframework.stereotype.Service;

//Importing CompletableFuture
import java.util.concurrent.CompletableFuture;

import java.util.ArrayList;
import java.util.List;

@Service
public class CraftServiceImpl implements CraftService {

    private String[] itemName = {
            "Dragon Breath", "Void Rhapsody", "Determination Symphony",
            "Opera of Wasteland", "FIRE BIRD"
    };

    private  List<CraftItem> allItems = new ArrayList<>();

    @Override
    public CraftItem createItem(String itemName) {
        CraftItem craftItem;
        craftItem = creator(itemName);
        allItems.add(craftItem);
        return craftItem;
    }

    // @TODO convert all creational method to async using CompleteableFuture
    // @TODO Based on Design principe DRY, fix code duplication from running async

    //To Prevent Repetition, I created a new method
    private CraftItem creator(String name){
        //I make a method that can handle creation of all types of recipes
        //to fulfill dry principle
        try{
            CraftItem craftItem = new CraftItem(name);
            CompletableFuture<Void> make;
            switch (name) {
                case "Dragon Breath":
                    make = CompletableFuture.runAsync(() -> craftItem.addRecipes(new SilentField())).thenRunAsync(() -> craftItem.addRecipes(new FireCrystal()));
                    break;
                case "Void Rhapsody":
                    make = CompletableFuture.runAsync(() -> craftItem.addRecipes(new SilentField())).thenRunAsync(() -> craftItem.addRecipes(new PureWaterfall()));
                    break;
                case "Determination Symphony":
                    make = CompletableFuture.runAsync(() -> craftItem.addRecipes(new BlueRoseRainfall())).thenRunAsync(() -> craftItem.addRecipes(new PureWaterfall()));
                    break;
                case "Opera of Wasteland":
                    make = CompletableFuture.runAsync(() -> craftItem.addRecipes(new DeathSword())).thenRunAsync(() -> craftItem.addRecipes(new SilentField())).thenRunAsync(() -> craftItem.addRecipes(new FireCrystal()));
                    break;
                default: //FIRE BIRD
                    make = CompletableFuture.runAsync(() -> craftItem.addRecipes(new FireCrystal())).thenRunAsync(() -> craftItem.addRecipes(new BirdEggs()));
                    break;
            }
            while (!make.isDone()){
                ;
            }
            craftItem.composeRecipes();
            return craftItem;
        }catch (Exception e){
            return creator("Opera of Wasteland");
        }
    }

    @Override
    public String[] getItemNames() {
        return itemName;
    }

    @Override
    public List<CraftItem> findAll() {
        return allItems;
    }
}
