# Demo Tutorial Lab 5
- Fransiscus Emmanuel Bunaren
- 1806173506

## How I implemented Async
I used CompletableFuture to run some methods Asynchronously. 
For example for Dragon Breath, I used this code


```
CompletableFuture<Void> make;

make = CompletableFuture.runAsync(() -> craftItem.addRecipes(new SilentField())).thenRunAsync(() -> craftItem.addRecipes(new FireCrystal()));
``` 

The code above creates a Variable "make" with type of CompletableFuture. 
After that, I used ```runAsync``` to run ```addRecipes``` asynchronously. I also used ```thenRunAsync```
to call another ```addRecipes``` asynchronously.

After that, I used ```While loop``` to wait until the Asynchronous execution in ```make``` variable is finished. Then, its is followed by ```composeRecipe``` execution.

Here is the code :

```
while (!make.isDone()){
    ;
}
craftItem.composeRecipes();
return craftItem;
```

## Why  the implemented code is Asynchronous?
Because, the ```addRecipe``` method is called simultaneously (at the same time).
So it is actually asynchronous (not sequential).

## Flow of the program
If we see in the original code, the method ```addRecipes``` is called multiple times sequentially.
We know that it can be called asynchronously to save time.

So to fix this, I used ```CompletableFuture``` to run the ```addRecipes``` methods in asynchronous way. 
So that, the ```addRecipes``` method does not need to wait for another ```addRecipes``` method to finish the execution.

After calling all the required ```addRecipes``` methods asynchronously, I waited until those methods has been done.

#### In short here is how the program works:

Run all ```addRecipes``` asynchronously > Wait until the asynchronous executions has been done. > Execute ```composeRecipes``` > return the object.