package id.ac.ui.cs.advprog.tutorial7.strategy.core;

public class Magician {
	
	private String name;
	private AttackAction attackAction;
	private DefenseAction defenseAction;
	private SupportAction supportAction;

	// DONE: The constructor still needs additional parameter. Add those needed parameters
	public Magician(String name,AttackAction attackAction,DefenseAction defenseAction,SupportAction supportAction){
		this.name = name;
		// DONE: initialization for other parameters
		this.attackAction = attackAction;
		this.defenseAction = defenseAction;
		this.supportAction = supportAction;
	}

	public Magician(){

	}
	
	// DONE : Change the implementation so it can get magician's attack action
	// Hint : Finish the first task (add additional constructor parameter) first
	public AttackAction getAttackAction(){
		return this.attackAction;
	}
	
	// DONE : Implement changing AttackAction
	// Hint : Finish the first task (add additional constructor parameter) first
	public void setAttackAction(AttackAction param){
		this.attackAction = param;
	}
	
	// DONE : Change the implementation so it can get magician's defense action
	// Hint : Finish the first task (add additional constructor parameter) first
	public DefenseAction getDefenseAction(){
		return this.defenseAction;
	}
	
	// DONE : Implement changing DefenseAction
	// Hint : Finish the first task (add additional constructor parameter) first
	public void setDefenseAction(DefenseAction param){
			this.defenseAction = param;
	}
	
	// DONE : Change the implementation so it can get magician's support action
	// Hint : Finish the first task (add additional constructor parameter) first
	public SupportAction getSupportAction(){
		return this.supportAction;
	}
	
	// DONE: Implement changing SupportAction
	// Hint : Finish the first task (add additional constructor parameter) first 
	public void setSupportAction(SupportAction param){
		this.supportAction = param;
	}
	
	// DONE : Implement this method
	public String attack(){
		return attackAction.attack();
	}
	
	// DONE : Implement this method
	public String defense(){
		return this.defenseAction.defense();
	}
	
	// DONE : Implement this method
	public String support(){
		return this.supportAction.support();
	}
	
	public String getName(){
		return name;
	}


}