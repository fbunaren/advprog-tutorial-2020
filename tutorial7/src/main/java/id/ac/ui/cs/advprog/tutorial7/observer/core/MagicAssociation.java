package id.ac.ui.cs.advprog.tutorial7.observer.core;

import java.util.ArrayList;
import java.util.List;

public class MagicAssociation {
        private List<Researcher> researchers = new ArrayList<>();
        private MagicResearch magicResearch;

        public void add(Researcher researcher) {
                researchers.add(researcher);
        }

        public void addResearch(MagicResearch magicResearch) {
                this.magicResearch = magicResearch;
                broadcast();
        }

        public String getResearchType () {return magicResearch.getType();}

        public MagicResearch getResearch() {return magicResearch;}

        public List<Researcher> getResearchers() {
                return researchers;
        }

        private void broadcast() {
                //DONE: Complete Me
                for (int i=0;i<researchers.size();i++){
                        researchers.get(i).update();
                }
        }
}
