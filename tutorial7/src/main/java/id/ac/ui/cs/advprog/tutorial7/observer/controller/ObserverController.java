package id.ac.ui.cs.advprog.tutorial7.observer.controller;

import id.ac.ui.cs.advprog.tutorial7.observer.core.MagicResearch;
import id.ac.ui.cs.advprog.tutorial7.observer.service.MagicAssociationServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class ObserverController {

        @Autowired
        private MagicAssociationServiceImpl magicAssociationService;

        @RequestMapping(value = "/create-research", method = RequestMethod.GET)
        public String createResearch(Model model){
                model.addAttribute("research", new MagicResearch());
                return "observer/researchForm";
        }

        @RequestMapping(value = "/add-research", method = RequestMethod.POST)
        public String addResearch(@ModelAttribute("research") MagicResearch magicResearch) {
                magicAssociationService.addResearch(magicResearch);
                // DONE redirect this to researcher-list URl
                return "redirect:/researcher-list";
        }

        @RequestMapping(value = "/researcher-list", method = RequestMethod.GET)
        public String getResearchers(Model model){
                model.addAttribute("researchers", magicAssociationService.getResearchers());
                return "observer/researcherList";
        }
}
