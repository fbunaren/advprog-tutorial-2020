package id.ac.ui.cs.advprog.tutorial7.observer.core;

public class Arcanist extends Researcher {

        public Arcanist(MagicAssociation magicAssociation) {
                this.name = "Arcanist";
                //DONE: Complete Me
                this.magicAssociation = magicAssociation;
        }

        public void update() {
                //DONE: Complete Me
                this.getResearchList().add(this.magicAssociation.getResearch());
        }

}
