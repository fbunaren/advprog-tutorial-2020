package id.ac.ui.cs.advprog.tutorial7.strategy.core;

public class Spellcaster extends Magician {
	
	// DONE : Equip this class with actions:
	// AttackAction : MagicMissile
	// DefenseAction : Barrier
	// SupportAction : Regenerate
	// Hint : Finish completing Magician class constructor first
	public Spellcaster (String name){
		super(name,new MagicMissile(),new Barrier(),new Regenerate());
	}
}