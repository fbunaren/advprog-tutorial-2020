package id.ac.ui.cs.advprog.tutorial7.strategy.core;

public class Enhancer extends Magician {
	
	// DONE : Equip this class with actions:
	// AttackAction : MagicMissile
	// DefenseAction : Barrier
	// SupportAction : Enhancer
	// Hint : Finish completing Magician class constructor first
	public Enhancer(String name){
		super(name,new MagicMissile(),new Barrier(),new Enhance());
	}
}