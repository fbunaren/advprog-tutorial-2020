package id.ac.ui.cs.advprog.tutorial7.observer.core;

public class Elementalist extends Researcher {

        public Elementalist(MagicAssociation magicAssociation) {
                this.name = "Elementalist";
                //DONE: Complete Me
                this.magicAssociation = magicAssociation;
        }

        public void update() {
                //DONE: Complete Me
                this.getResearchList().add(this.magicAssociation.getResearch());
        }
}
