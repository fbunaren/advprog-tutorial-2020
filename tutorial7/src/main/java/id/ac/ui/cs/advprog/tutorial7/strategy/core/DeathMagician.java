package id.ac.ui.cs.advprog.tutorial7.strategy.core;

public class DeathMagician extends Magician {
	
	// DONE : Equip this class with actions:
	// AttackAction : Harvest
	// DefenseAction : Barrier
	// SupportAction : Regenerate
	// Hint : Finish completing Magician class constructor first
	public DeathMagician(String name){
		super(name,new Harvest(),new Barrier(),new Regenerate());
	}
}