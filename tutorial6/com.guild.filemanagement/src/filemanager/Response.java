package filemanager;

public class Response<T> {
    private T content;
    private String message;
    private int status;

    public void setContent(T content) {
        this.content = content;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public T getContent() {
        return content;
    }

    public String getMessage() {
        return message;
    }

    public int getStatus() {
        return status;
    }
}
