#Tutorial 3 Advanced Programming
Fransiscus Emmanuel Bunaren (1806173506)

- Jelaskan Flow dari Program

Program ini pada awalnya menginisialisasi mana dengan nilai 4 seperti yang ada di file Manalith.java.  
Kemudian, seperti yang dapat dilihat di file Synthesis, setiap kali user membuat request, akan terjadi pemanggilan `requestMana` di file Synthesis.java.
Kemudian, saat Process Request di klik, maka processRequest di `ManalithServiceImpl` dipanggil dan method tersebut membuat thread dengan menggunakan runable dan memanggil runManalith.

- Identifikasi Race Condition

Race condition terjadi di dalam file `ManalithServiceImpl.java`

Jika kita lihat dalam `startThread`, 
ia menggunakan `runable`  untuk menjalankan `runManalith` dalam thread. 
Namun, dalam method `processRequest()` , terjadi pemanggilan `startThread` yang diikuti dengan `runManalith`. Pemanggilan tersebut redundant (berlebihan) 
karena `startThread` sendiri sudah menjalankan `runManalith` dalam thread dan kemudian pada line berikutnya `runManalith` kembali dipanggil. 

Selain itu, terjadinya race condition, juga disebabkan karena thread-thread yang dibuat berjalan secara bersamaan.

- Jelaskan kenapa race condition terjadi

Sama seperti penjelasan diatas, terjadi pemanggilan method `runManalith` secara redundant(berlebihan / berulang-ulang) 
sehingga operasi yang sama dijalankan lebih dari sekali. 
Selain itu, penbuatan thread lebih dari satu mengakibatkan satu data diakses secara bersamaan oleh beberapa thread, sehingga terjadi race condition.

- Solusi supaya program berjalan dengan benar

Pemanggilan method yang redundant dihilangkan. Kemudian, setiap kali pemanggilan thread, dapat ditambahkan `Locking mechanism` yang mencegah suatu data diakses oleh secara bersamaan, contohnya dengan memberi `flag/penanda` bahwa suatu data sudah diakses. Selain itu, mengubah struktur data yang digunakan menjadi struktur data yang "aman" terhadap thread. Struktur data queue, stack, dan linked list tidaklah "Thread Safe". Sementara HashTable, dll adalah struktur data yang thread safe.

