package id.ac.ui.cs.tutorial3.core;

import java.util.LinkedList;
import java.util.Queue;

public class Manalith {
	public int mana;
	private Queue<Synthesis> requestQueue;

	public Manalith(int initMana, Queue<Synthesis> requestQueue){
		this.mana = initMana;
		this.requestQueue = requestQueue;
	}

	public Manalith(){
		this(4, new LinkedList<>());
	}

	public int getMana(){
		return this.mana;
	}

	public boolean noRequest(){
		return requestQueue.isEmpty();
	}

	public void addRequest(Synthesis synth){
		requestQueue.offer(synth);
	}

	public synchronized String processRequest(){
		String res;
		Synthesis synth = requestQueue.poll();
		try {
			int manaGain = synth.getManaGain();
			boolean respondReport = false;
			if (mana >= manaGain) {
				respondReport = synth.respond();
				if (synth.respond()) {
					mana = mana - manaGain;
				}
			}

			res = synth.requestMessage() + ", Status = " + respondReport + ", Manalith Mana = " + this.mana;
		}catch (Exception e){
			res = "";
		}
		return res;
	}

	@Override
	public String toString(){
		return "Manalith, mana = " + this.mana;
	}
}