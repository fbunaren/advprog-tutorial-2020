package id.ac.ui.cs.tutorial3.service;

import java.util.List;
import java.util.ArrayList;

import id.ac.ui.cs.tutorial3.core.Manalith;
import id.ac.ui.cs.tutorial3.core.Synthesis;
import id.ac.ui.cs.tutorial3.repository.SynthesisRepository;
import org.springframework.stereotype.Service;

@Service
public class ManalithServiceImpl implements ManalithService {
	private final SynthesisRepository synthRepo;
	private final Manalith manalith;

	public ManalithServiceImpl(SynthesisRepository synthRepo, Manalith manalith){
		this.synthRepo = synthRepo;
		this.manalith = manalith;
	}

	public ManalithServiceImpl(SynthesisRepository synthRepo){
		this(synthRepo, new Manalith());
	}

	public ManalithServiceImpl(){
		this(new SynthesisRepository(), new Manalith());
	}

	@Override
	public void addSynth(Synthesis synth){
		synthRepo.addSynthesis(synth);
	}

	@Override
	public List<Synthesis> getSynthesises(){
		return synthRepo.getSynthesises();
	}

	@Override
	public List<String> processRequest(){
		List<String> statusList = new ArrayList<>();
		createRequestFromRepo();
		startThread(statusList);
		runManalith(statusList);
		synthRepo.emptyRepo();
		return statusList;
	}

	private void createRequestFromRepo(){
		List<Synthesis> synthList = getSynthesises();
		//Making Flag for preventing Race Conditions
		ArrayList<Boolean> flag = new ArrayList<>();
		for (int i=0;i<synthList.size();i++){
			flag.add(false);
		}
		int iter = 0;
		for(Synthesis synth : synthList){
			if (!flag.get(iter) && !synth.processed) {
				flag.set(iter,true);
				synth.requestMana(manalith);
				iter++;
			}
		}
	}

	private void startThread(List<String> statusList){
		new Thread(new Runnable(){
			public void run(){
				runManalith(statusList);
			}
		}).start();
	}

	private void runManalith(List<String> statusList){
		while(!manalith.noRequest()){
			statusList.add(manalith.processRequest());
		}
	}
}